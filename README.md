# CrypticAnt

![A screenshot of the UI](https://i.ibb.co/n7y3mhX/Screenshot-2021-07-23-at-11-10-19.png)

CrypticAnt is an application, that has the goal of making conversions between encoded/encrypted text.

It is build around the idea, of working similarly to Google Translate. You can select the data type, and then type in either of the textareas to convert between the two.

> The application can be accessed here: https://antexperiments.gitlab.io/webapps/cryptic-ant/
